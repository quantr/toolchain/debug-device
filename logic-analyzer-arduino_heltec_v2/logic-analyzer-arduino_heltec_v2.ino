// Turn arduino into logic analyzer
// Author: Peter <peter@quantr.hk
// Homepage: https://gitlab.com/quantr/toolchain/debug-device/-/tree/main/logic-analyzer-arduino_heltec_v2

#include <Wire.h>
#include "heltec.h"
#include "quantr_logo.h"

int start = 0;
int ledState = LOW;
const long interval = 10;
unsigned long previousMillis = 0;
unsigned long previousMillis2 = 0;
unsigned long long samplingRate;
unsigned long long limit;
unsigned long noOfSample;

String pattern1 = "set sample rate:";
String pattern2 = "set limit:";

void clearLine(int lineNo) {
  Heltec.display->setColor(BLACK);
  Heltec.display->fillRect(0, lineNo * 11, 200, 11);
}

void printLine(int lineNo, String str) {
  clearLine(lineNo);
  Heltec.display->setColor(WHITE);
  Heltec.display->drawString(0, lineNo * 11, str);
  Heltec.display->display();
}

void setup() {
  Heltec.begin(true /*DisplayEnable Enable*/, false /*LoRa Disable*/, true /*Serial Enable*/);
  Heltec.display->flipScreenVertically();
  Heltec.display->setFont(ArialMT_Plain_10);

  printLine(0, "Quantr Debug Device");

  //  Heltec.display->drawXbm(0, 13,  WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  //  Heltec.display->display();

  Serial.begin(115200);
}

String convert(unsigned long x) {
  if (x >= 1000 * 1000 * 1000) {
    return String(x / 1000 / 1000 / 1000) + "G";
  } else if (x >= 1000 * 1000) {
    return String(x / 1000 / 1000) + "M";
  } else if (x >= 1000) {
    return String(x / 1000) + "K";
  } else {
    return String (x);
  }
}

String convert(unsigned long long x) {
  char buf[50];

  if (x >= 1000 * 1000 * 1000 * 1000) {
    sprintf(buf, "%llu", x / 1000 / 1000 / 1000);
    return String(buf) + "G";
  } else if (x >= 1000 * 1000 * 1000) {
    sprintf(buf, "%llu", x / 1000 / 1000 / 1000);
    return String(buf) + "G";
  } else if (x >= 1000 * 1000) {
    sprintf(buf, "%llu", x / 1000 / 1000);
    return String(buf) + "M";
  } else if (x >= 1000) {
    sprintf(buf, "%llu", x / 1000);
    return String(buf) + "K";
  } else {
    sprintf(buf, "%llu", x);
    return String(buf);
  }
}

unsigned long long stringToUnsignedLongLong(String s)
{
  return strtoull(s.c_str(), nullptr, 10);
  //  unsigned long long x=0;
  //  for (int x=0;x<s.length();x++){
  //    if (x>0){
  //      x=x<<8;
  //    }
  //    x+=s.substring(x,x+1).toInt();
  //  }
  //  return x;
}

void loop() {
  if (start == 0) {
    String c = Serial.readStringUntil('\n');
    if (c == "s") {
      start = 1;
      noOfSample = 0;
      clearLine(3);
      Serial.println("ok");
    } else if (c == "e") {
      start = 0;
      Serial.println("ok");
    } else if (c == "ping") {
      Serial.println("Quantr debug device");
    } else if (c == "scan") {
      Serial.println("quantr,8,0");
    } else if (c.startsWith(pattern2)) {
      String temp = c.substring(pattern2.length(), c.length());
      limit = stringToUnsignedLongLong(temp);
      printLine(1, "Limit: " + convert(limit) + " samples");
      //    printLine(3, "temp: " + temp);
      Serial.println("ok");
    } else if (c.startsWith(pattern1)) {
      String temp = c.substring(pattern1.length(), c.length());
      samplingRate = temp.toInt();
      printLine(2, "Rate: " + convert(samplingRate) + "Hz");
      Serial.println("ok");
    }
  } else if (start == 1) {
    unsigned long currentMillis = millis();
    if ((currentMillis - previousMillis2) >= (1000 / samplingRate)) {
      previousMillis2 = currentMillis;
      //      Serial.write(noOfSample % 8);
      printLine(3, "Capturing: " + String(noOfSample));
      noOfSample++;
    }
    if (noOfSample == limit) {
      Serial.println("finish");
      printLine(3, "Finish");
      start = 0;
    }
  }
}
