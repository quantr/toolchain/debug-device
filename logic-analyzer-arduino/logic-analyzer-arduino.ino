// This is a logic analyzer
// Author: Peter <peter@quantr.hk
// Homepage: https://gitlab.com/quantr/toolchain/debug-device/-/tree/main/logic-analyzer-arduino

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

int start=0;
int ledState = LOW;
const long interval = 10;
unsigned long previousMillis = 0;
unsigned long previousMillis2 = 0;
LiquidCrystal_I2C lcd(0x27,16,2); // set the LCD address to 0x27 for a 16 chars and 2 line display
unsigned long samplingRate;
unsigned long limit;
unsigned noOfSample;

String pattern1="set sample rate:";
String pattern2="set limit:";

void setup() {
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Quantr debug");
  
  Serial.begin(115200);
  for (int x=0;x<=13;x++){
    pinMode(x, OUTPUT);
  }
}

void clearLine(int x){
  lcd.setCursor(0, x);
  lcd.print("                ");
}

void loop() {
  String c = Serial.readStringUntil('\n');
  if (c=="s"){
    start=1;
    noOfSample=0;
    Serial.println("ok");
    clearLine(0);
    clearLine(1);
  }else if (c=="e"){
    start=0;
    Serial.println("ok");
    for (int x=0;x<=13;x++){
      digitalWrite(x, LOW);
    }
  }else if (c=="ping"){
    Serial.println("Quantr debug device");
  }else if (c=="scan"){
    Serial.println("quantr,8,0");
  }else if (c.startsWith(pattern1)){
    clearLine(0);
    lcd.setCursor(0, 0);
    lcd.print("Sample rate:");
    lcd.print(c.substring(pattern1.length(), c.length()));
    samplingRate=c.substring(pattern1.length(), c.length()).toInt();
    Serial.println("ok");
  }else if (c.startsWith(pattern2)){
    clearLine(1);
    lcd.setCursor(0, 1);
    lcd.print("Limit:");
    lcd.print(c.substring(pattern2.length(), c.length()));
    limit=c.substring(pattern2.length(), c.length()).toInt();
    Serial.println("ok");
  }
  if (start==1){
    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      
      if (ledState == LOW) {
        ledState = HIGH;
      } else {
        ledState = LOW;
      }
      
      for (int x=0;x<=13;x++){
        digitalWrite(x, ledState);
      }
    }
    if (currentMillis - previousMillis2>=1000/samplingRate){
      previousMillis2 = currentMillis;
      Serial.write(noOfSample%8);
//      clearLine(0);
//      lcd.print("Capturing: ");
//      lcd.print(noOfSample);
      noOfSample++;
    }
    if (noOfSample==limit){
      Serial.println("finish");
      for (int x=0;x<=13;x++){
        digitalWrite(x, LOW);
      }
    }
  }
}
