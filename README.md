# Logic Analyzer Device

We are a highly configurable logic analyzer device to support our RISC-V cpu development. So we decided to build one, it will be the co-operate project with CityU and lead by [Professor Ray](https://www.ee.cityu.edu.hk/~rcheung/About_Me.html)

# Our goal

Create a logic analyzer:

1. support [sigrok](https://sigrok.org/)
1. support [jtag protocol](https://en.wikipedia.org/wiki/JTAG)
1. support direct [gdb protocol](https://www.embecosm.com/appnotes/ean4/embecosm-howto-rsp-server-ean4-issue-2.html)
1. support [RISC-V External Debug Support](https://riscv.org/wp-content/uploads/2019/03/riscv-debug-release.pdf)
1. support [RISC-V Trace Specification](https://github.com/riscv/riscv-trace-spec/raw/e372bd36abc1b72ccbff31494a73a862367cbb29/riscv-trace-spec.pdf)

# Target platform

We stick to Xilinx ZYNQ

1. Board Cora Z7 ![](https://res.cloudinary.com/rsc/image/upload/b_rgb:FFFFFF,c_pad,dpr_2.0,f_auto,h_449,q_auto,w_800/c_pad,h_449,w_800/Y1840456-01?pgw=1)
1. Board [FPGA XILINX Board](https://zhengdianyuanzi.tmall.com/category-1597679519.htm?spm=a220o.1000855.0.0.66207740Rlgfi7&search=y&parentCatId=1498161506&parentCatName=FPGA%2FZYNQ%BF%AA%B7%A2%B0%E5&catName=FPGA%2FZYNQ%BA%CB%D0%C4%B0%E5%CF%B5%CD%B3%B0%E5)

![FPGA XILINX Board](https://img.alicdn.com/imgextra/i4/2206849645613/O1CN01lqz6vW1rKnpplkIVn_!!0-item_pic.jpg_430x430q90.jpg)

# Skillset we need

1. Xilinx Zynx coding
   1. control the clock and capture pin signal at very precise 
   1. use USB or TTL-to-USB to send back signal to PC
   1. record all data into Zynx's memory
   1. real time capture mode
   1. In furture we may provide embed LCD display in our logic analyzer device, so need to know how to code it
1. libsigrok coding. Need to know how to feed the data into [pulseview](https://sigrok.org/wiki/PulseView)
1. java coding, we use java as primary language to code the logic analyzer part. Java can direct read com-port and USB now. We build debug interface and debug language using [Antlr](https://www.antlr.org/)
1. GDB protocol coding. I got my own library [JLibGDB](https://gitlab.com/quantr/toolchain/JLibGDB) to parse the bytes in GDB protocol
1. Understand and actual implement JTag in RISC-V and in PC side, encode and decode the jtag protocol
1. Create a debug language and able to input to our device. Support breakpoint, catch point, watch point, etc.
