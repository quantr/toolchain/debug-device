#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>

int fd;

int write_port(int fd, uint8_t *buffer, size_t size) {
    ssize_t result = write(fd, buffer, size);
    if (result != (ssize_t)size) {
        perror("failed to write to port");
        return -1;
    }
    return 0;
}

ssize_t read_port(int fd, uint8_t *buffer, size_t size) {
    size_t received = 0;
    while (received < size) {
        ssize_t r = read(fd, buffer + received, size - received);
        if (r < 0) {
            perror("failed to read from port");
            return -1;
        }
        if (r == 0) {
            // Timeout
            break;
        }
        received += r;
    }
    return received;
}

char * send(char *str){
    write_port(fd, (uint8_t *)str, strlen(str));

    char *buffer=(char *)malloc(20);
    read_port(fd, (uint8_t *)buffer, 20);
	return buffer;
}

int main() {
#ifdef __APPLE__
    // char *device = "/dev/cu.wchusbserial1440";
//	char *device = "/dev/cu.wchusbserial1410";
    char *device="/dev/cu.SLAB_USBtoUART";
    fd = open(device, O_RDWR | O_NOCTTY);
    if (fd == -1) {
        perror(device);
        return -1;
    }

    // Flush away any bytes previously read or written.
    int result = tcflush(fd, TCIOFLUSH);
    if (result) {
        perror("tcflush failed");  // just a warning, not a fatal error
    }

    // Get the current configuration of the serial port.
    struct termios userOptions;
    result = tcgetattr(fd, &userOptions);
    if (result) {
        perror("tcgetattr failed");
        close(fd);
        return -1;
    }

    // Turn off any options that might interfere with our ability to send and
    // receive raw binary bytes.
    userOptions.c_iflag &= ~(INLCR | IGNCR | ICRNL | IXON | IXOFF);
    userOptions.c_oflag &= ~(ONLCR | OCRNL);
    userOptions.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);

    // Set up timeouts: Calls to read() will return as soon as there is
    // at least one byte available or when 100 ms has passed.
    userOptions.c_cc[VTIME] = 1;
    userOptions.c_cc[VMIN] = 0;

    // This code only supports certain standard baud rates. Supporting
    // non-standard baud rates should be possible but takes more work.
    cfsetospeed(&userOptions, B115200);
    cfsetispeed(&userOptions, cfgetospeed(&userOptions));

    result = tcsetattr(fd, TCSANOW, &userOptions);
    if (result) {
        perror("tcsetattr failed");
        close(fd);
        return -1;
    }

    //sleep(3);

	char *buffer=send("ping\n");
    printf("buffer=%s\n", buffer);
	free(buffer);

	buffer=send("set sample rate:1234\n");
    printf("buffer=%s\n", buffer);
	free(buffer);


    close(fd);
#endif
    return 0;
}
