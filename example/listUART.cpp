#include <dirent.h>
#include <stdio.h>

#include <filesystem>
#include <iostream>
#include <string>
using namespace std;

int main() {
#ifdef __APPLE__
	struct dirent *d;
    DIR *dr;
    dr = opendir("/dev");
    if(dr!=NULL)
    {
        for(d=readdir(dr); d!=NULL; d=readdir(dr))
        {
			if (strncmp("cu.", d->d_name, 3) == 0){
            	cout<<d->d_name<<endl;
			}
        }
        closedir(dr);
    }
#endif
    return 0;
}
