#!/usr/local/bin/python3
 
import sys
import png

def exportCArray(list, width):
	tempByte=0
	tempOffset=7
	tempX=0
	for x in range(0, len(list)):
		tempByte|=(list[x]<<(7-tempOffset))
		if tempOffset==0:
			print("0x"+format(tempByte, '02x')+" ", end =",")
			tempByte=0
			tempOffset=7
			tempX+=1
		else:
			tempOffset-=1
		if x % width==0:
			print("")

def printBitmap(list, width):
	tempByte=0
	tempOffset=7
	tempX=0
	for x in range(0, len(list)):
		tempByte|=(list[x]<<tempOffset)
		if tempOffset==0:
			print(format(tempByte, '08b'), end ="")
			tempByte=0
			tempOffset=7
			tempX+=1
		else:
			tempOffset-=1
		if x % width==0:
			print("")

if len(sys.argv)!=2:
	print('Please input png file name')
	sys.exit()

r=png.Reader(filename=sys.argv[1])
info=r.read()
rows=list(info[2])
print(rows)

mylist=[]
for row in rows:
	# print("--------------------------------------------------------", len(row))
	for x in range(0, len(row), 4):
		byte0=row[x]
		byte1=row[x+1]
		byte2=row[x+2]
		byte3=row[x+3]

		print(byte0, ' ', byte1, ' ', byte2, ' ', byte3)

		if byte3==0:
			mylist.append(0)
			# print(" ",end="")
		else:
			mylist.append(1)
			# print("x",end="")
	# print("")

printBitmap(mylist, 48)
exportCArray(mylist, 48)

